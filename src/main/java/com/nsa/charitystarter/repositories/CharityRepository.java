package com.nsa.charitystarter.repositories;

import com.nsa.charitystarter.entities.Charity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Used to access the Charity table
 */
@Repository
public interface CharityRepository extends JpaRepository<Charity, Integer> {
}
