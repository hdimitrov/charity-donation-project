package com.nsa.charitystarter.repositories;

import com.nsa.charitystarter.dtos.CharityDetailsDTO;
import com.nsa.charitystarter.dtos.CharityInListDTO;
import com.nsa.charitystarter.services.CharityServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CharityDetailsRepositoryImpl implements CharityDetailsRepository {
    static final Logger LOG = LoggerFactory.getLogger(CharityDetailsRepositoryImpl.class);

    @Autowired
    public CharityDetailsRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private JdbcTemplate jdbcTemplate;
    @Value("${sql.charity.details_one}")
    private String getSingleCharityDTOByID;

    /**
     * Fetches a single charity's name, donation amount, and amount of gift aid achieved.
     *
     * @param id the ID of the charity we're retrieving
     * @return an Optional of the charity
     */
    public Optional<CharityDetailsDTO> getCharityDTOByID(int id) {
        LOG.info("returning charity DTO");
        List<CharityDetailsDTO> match = jdbcTemplate.query(getSingleCharityDTOByID,
                new Object[]{id},
                (rs, i) ->
                        new CharityDetailsDTO(
                                rs.getString("name"),
                                rs.getInt("amount_no_gift_aid"),
                                rs.getInt("gift_aid_amount")
                        ));

        return match.stream().findFirst();
    }

    /**
     * Get the chariaties that match the params
     *
     * @param minimumDonationAmount the minimum donation amount
     * @param minimumDonationCount  the minimum donation count
     * @param name                  part of the name of the charity
     * @param limitStart            the start of the indexing
     * @param limitCount            where we end the limit
     * @param orderBy               what we're sorting by
     * @param orderDirection        asc/desc depending on how we're sorting
     * @return a list of charities that match the criteria
     */

    public List<CharityInListDTO> getCharitiesInList(
            int minimumDonationAmount,
            int minimumDonationCount,
            String name,
            int limitStart,
            int limitCount,
            String orderBy,
            String orderDirection
    ) {
        LOG.info("Matching charities");
        var match = jdbcTemplate.query(
                "SELECT \n" +
                        "charity.id,\n" +
                        " name,\n" +
                        "IFNULL(SUM(CASE WHEN is_own_money = 1 AND has_no_benefit_to_donor = 1 AND wishes_to_gift_aid = 1 AND address.country_iso_code IN ('GB','UK') THEN amount_in_pence*1.2 ELSE amount_in_pence END),0) as total_amount,\n" +
                        "COUNT(donation.id) as number_of_donations\n" +
                        " FROM donation\n" +
                        " RIGHT JOIN charity on donation.charity_id = charity.id\n" +
                        "LEFT JOIN donor on donation.donor_id=donor.id " +
                        "LEFT JOIN address on address.id=donor.address_id " +
                        "GROUP BY charity.id \n" +
                        "HAVING name LIKE ? AND total_amount >= ? AND number_of_donations>=?\n" +
                        "\n" +
                        "ORDER BY " + orderBy + " " + orderDirection + " " +
                        "LIMIT ?,?",
                new Object[]{"%" + name + "%", minimumDonationAmount, minimumDonationCount, limitStart, limitCount},
                (rs, i) ->
                        new CharityInListDTO(
                                rs.getString("name"),
                                rs.getInt("id"),
                                rs.getInt("total_amount"),
                                rs.getInt("number_of_donations")
                        )
        );
        return match;
    }
}
