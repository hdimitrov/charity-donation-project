package com.nsa.charitystarter.repositories;

import com.nsa.charitystarter.dtos.CharityDetailsDTO;
import com.nsa.charitystarter.dtos.CharityInListDTO;

import java.util.List;
import java.util.Optional;

public interface CharityDetailsRepository {

    Optional<CharityDetailsDTO> getCharityDTOByID(int id);

    List<CharityInListDTO> getCharitiesInList(
            int minimumDonationAmount,
            int minimumDonationCount,
            String name,
            int limitStart,
            int limitCount,
            String orderBy,
            String orderDirection
    );
}
