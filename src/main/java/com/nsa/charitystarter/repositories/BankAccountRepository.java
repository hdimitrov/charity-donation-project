package com.nsa.charitystarter.repositories;

import com.nsa.charitystarter.entities.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Used to access the bank account table
 */
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Integer> {
}
