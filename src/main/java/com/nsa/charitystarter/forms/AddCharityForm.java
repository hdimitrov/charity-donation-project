package com.nsa.charitystarter.forms;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Contains the fields for the AddCharity form. this is its own object as  it represents two DB entries
 * A Charity and a bank account
 */
@Data
@AllArgsConstructor
public class AddCharityForm {
    /**
     * Stores the charity's registration ID
     */
    @NotEmpty
    private String registrationID;
    /**
     * Stores the charity's name (acronym is generated off the name)
     */
    @NotEmpty
    private String name;
    /**
     * Stores the charity's purpose
     */
    @NotEmpty
    private String purpose;

    /**
     * Stores the charity bank account's sort code
     */
    @Digits(integer = 10, fraction = 0)
    private Integer sortCode;

    /**
     * Stores the charity bank account's account number
     */
    @Digits(integer = 12, fraction = 0)
    private Integer accountNumber;
}
