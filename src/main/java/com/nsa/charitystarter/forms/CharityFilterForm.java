package com.nsa.charitystarter.forms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

/**
 * Contains the charity filter form
 */
@Data
@NoArgsConstructor
public class CharityFilterForm {
    /**
     * The value for the filter by minimum amount
     */
    private int minimumDonationAmountFilter = 0;
    /**
     * The value for the filter by name
     */
    private String nameFilter = "";
    /**
     * The value for the filter by minimum donation
     */
    private int minimumDonationCountFilter = 0;
    /**
     * The value for the starting page
     */
    @PositiveOrZero
    private int startingPage = 0;
    /**
     * The value for the number of items per page
     */
    @Min(5)
    @Max(20)
    private int itemsPerPage = 10;
    /**
     * What we're sorting by.
     */
    @Pattern(regexp = "^name|total_amount|number_of_donations$")
    private String sortBy = "total_amount";
    /**
     * Stores the direction of the sort
     */
    @Pattern(regexp = "^asc|desc$", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String sortDirection = "DESC";
}
