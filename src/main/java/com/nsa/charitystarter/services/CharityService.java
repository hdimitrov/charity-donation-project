package com.nsa.charitystarter.services;

import com.nsa.charitystarter.dtos.CharityDetailsDTO;
import com.nsa.charitystarter.dtos.CharityInListDTO;
import com.nsa.charitystarter.entities.BankAccount;
import com.nsa.charitystarter.entities.Charity;

import java.util.List;
import java.util.Optional;

/**
 * Interface for the charity service
 */
public interface CharityService {
    /**
     * Adds a charity
     *
     * @param charity     the charity in question
     * @param bankAccount its initial bank account
     */
    void addCharity(Charity charity, BankAccount bankAccount);


    /**
     * Gets the details of a charity by its ID
     *
     * @param id the id of the charity
     * @return the charity fetched
     */
    Optional<CharityDetailsDTO> getCharityDetailsByID(int id);

    List<CharityInListDTO> getCharitiesFiltered(
            int minimumDonationAmount,
            int minimumDonationCount,
            String name,
            int pageNumber,
            int limitCount,
            String orderBy,
            String orderDirection);
}
