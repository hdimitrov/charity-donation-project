package com.nsa.charitystarter.services;

import com.nsa.charitystarter.CharityApp;
import com.nsa.charitystarter.dtos.CharityDetailsDTO;
import com.nsa.charitystarter.dtos.CharityInListDTO;
import com.nsa.charitystarter.entities.BankAccount;
import com.nsa.charitystarter.entities.Charity;
import com.nsa.charitystarter.repositories.BankAccountRepository;
import com.nsa.charitystarter.repositories.CharityDetailsRepository;
import com.nsa.charitystarter.repositories.CharityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Implementation of the CharityService class using repositories
 */
@Service
public class CharityServiceImpl implements CharityService {

    static final Logger LOG = LoggerFactory.getLogger(CharityServiceImpl.class);

    /**
     * Stores the instance of CharityRepository
     */
    private CharityRepository charityRepository;
    /**
     * Stores  the instance of BankAccountRepository
     */
    private BankAccountRepository bankAccountRepository;

    private CharityDetailsRepository charityDetailsRepository;

    /**
     * Autowired constructor for necessary repositories
     */
    @Autowired
    public CharityServiceImpl(CharityRepository charityRepository, BankAccountRepository bankAccountRepository, CharityDetailsRepository charityDetailsRepository) {
        this.charityRepository = charityRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.charityDetailsRepository = charityDetailsRepository;
    }


    /**
     * Implements adding a charity
     *
     * @param charity     the charity in question
     * @param bankAccount its initial bank account
     */
    @Override
    public void addCharity(Charity charity, BankAccount bankAccount) {
        LOG.info("Adding charity " + charity.getName());
        charityRepository.save(charity);
        bankAccount.setCharity(charity);
        bankAccountRepository.save(bankAccount);
    }

    /**
     * Gets the charity details by ID from the repo
     *
     * @param id the id of the charity
     * @return an optional of the charity, empty optional otherwise
     */
    @Override
    public Optional<CharityDetailsDTO> getCharityDetailsByID(int id) {
        LOG.info("Getting charity details");
        return charityDetailsRepository.getCharityDTOByID(id);
    }

    /**
     * Gets the list of charities filtered by this criteria
     *
     * @param minimumDonationAmount minimum total amount fo donations
     * @param minimumDonationCount  minimum count of donations
     * @param name                  string contained in the name
     * @param pageNumber            page number
     * @param limitCount            number of items epr page
     * @param orderBy               what it is sorted by
     * @param orderDirection        the direction of the sorting.
     * @return a list of charities that is filtered
     */
    public List<CharityInListDTO> getCharitiesFiltered(
            int minimumDonationAmount,
            int minimumDonationCount,
            String name,
            int pageNumber,
            int limitCount,
            String orderBy,
            String orderDirection) {
        return charityDetailsRepository.getCharitiesInList(
                minimumDonationAmount,
                minimumDonationCount,
                name,
                pageNumber * limitCount,
                limitCount,
                orderBy,
                orderDirection
        );

    }
}
