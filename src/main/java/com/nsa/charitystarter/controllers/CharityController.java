package com.nsa.charitystarter.controllers;

import com.nsa.charitystarter.services.CharityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Returns views relevant to Charities
 */
@RequestMapping(path = "/charities")
@Controller
public class CharityController {

    static final Logger LOG = LoggerFactory.getLogger(CharityController.class);

    /**
     * Shows the charity addition form.
     */
    @GetMapping("/add")
    public String charityAddForm() {
        LOG.info("Showing add charity form");
        return "add";
    }

    /**
     * Shows the single charity view
     */
    @GetMapping("/view/{id}")
    public String getOneCharity() {
        LOG.info("showing a single charity");
        return "view";
    }

    /**
     * Shows the viiew of multiple charities
     */
    @GetMapping("")
    public String getAlLCharities() {
        LOG.info("Showing all charities");
        return "list";
    }
}
