package com.nsa.charitystarter.controllers.api;

import com.nsa.charitystarter.apireturns.SimpleSuccess;
import com.nsa.charitystarter.apireturns.ValidationError;
import com.nsa.charitystarter.dtos.CharityDetailsDTO;
import com.nsa.charitystarter.dtos.CharityInListDTO;
import com.nsa.charitystarter.entities.BankAccount;
import com.nsa.charitystarter.entities.Charity;
import com.nsa.charitystarter.forms.AddCharityForm;
import com.nsa.charitystarter.forms.CharityFilterForm;
import com.nsa.charitystarter.repositories.CharityDetailsRepositoryImpl;
import com.nsa.charitystarter.services.CharityService;
import com.nsa.charitystarter.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller that handles the API calls for charities
 */
@RestController
@RequestMapping(path = "/api/charities")
public class CharityApiController {

    static final Logger LOG = LoggerFactory.getLogger(CharityApiController.class);
    /**
     * Autowired instance of charityService
     */
    private CharityService charityService;

    /**
     * Autowired constructor
     */
    @Autowired
    public CharityApiController(CharityService charityService) {
        this.charityService = charityService;
    }

    /**
     * Adds a charity or returns an error
     */
    @PostMapping(path = "")
    public ResponseEntity addCharity(@Valid AddCharityForm addCharityForm, BindingResult result) {
        // Return pattern from: https://stackoverflow.com/questions/44649032/best-practice-to-send-json-response-from-spring-controller-methods
        // Accessed on 22/10/2018
        if (result.hasErrors()) {
            // If there are any errors, format them nicely and return them
            LOG.info("Attempted to add charity but had an error");
            return new ResponseEntity<>(ValidationError.mapBindingResultToValidationError(result), HttpStatus.BAD_REQUEST);
        } else {
            // If there are no errors, create the charity.
            var c = new Charity();
            c.setPurpose(addCharityForm.getPurpose());
            c.setRegistrationNumber(addCharityForm.getRegistrationID());
            c.setName(addCharityForm.getName());
            // Reference: https://stackoverflow.com/questions/1583940/how-do-i-get-the-first-n-characters-of-a-string-without-checking-the-size-or-goi
            // Accessed  on 22/10/2018
            c.setAcronym(addCharityForm.getName().toUpperCase().substring(0, Math.min(5, c.getName().length())));
            var bankAccount = new BankAccount();
            bankAccount.setSortCode(addCharityForm.getSortCode());
            bankAccount.setAccountNumber(addCharityForm.getAccountNumber());
            charityService.addCharity(c, bankAccount);
            LOG.info("Adding charity successfully");
            return new ResponseEntity<>(new SimpleSuccess(), HttpStatus.OK);
        }
    }


    /**
     * Fetches a single charity by its ID
     *
     * @param id the ID of the charity
     * @return the charity, or a 404 if its not found.
     */
    @GetMapping("/{id}")
    public ResponseEntity getSingleCharity(@PathVariable int id) {
        LOG.info("Showing a single charity");
        var charityDetails = charityService.getCharityDetailsByID(id);
        if (charityDetails.isPresent()) {
            LOG.info("Charity retrieved successfully");
            return new ResponseEntity<>(charityDetails.get(), HttpStatus.OK);
        } else {
            LOG.info("Charity not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("")
    public ResponseEntity getListOfCharities(@Valid CharityFilterForm charityFilterForm, BindingResult result) {
        if (result.hasErrors()) {
            LOG.info("Requested charities but had an error");
            return new ResponseEntity<>(ValidationError.mapBindingResultToValidationError(result), HttpStatus.BAD_REQUEST);
        }
        LOG.info("Retrieving list of charities");

        return new ResponseEntity<>(charityService.getCharitiesFiltered(charityFilterForm.getMinimumDonationAmountFilter(), charityFilterForm.getMinimumDonationCountFilter(), charityFilterForm.getNameFilter(), charityFilterForm.getStartingPage(), charityFilterForm.getItemsPerPage(), charityFilterForm.getSortBy(), charityFilterForm.getSortDirection()), HttpStatus.OK);
    }

}
