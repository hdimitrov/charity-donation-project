package com.nsa.charitystarter.apireturns;


import com.nsa.charitystarter.utils.Pair;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Return type for listing fields with errors.
 */
@Data
@AllArgsConstructor
public class ValidationError {

    /**
     * Contains the ;ist of errors. the key for each error is the field
     * and the value is the error itself.
     */
    private List<Pair<String, String>> errors;

    /**
     * Builds a nicely  formamtted error return from a BindingResult
     * @param result the binding result
     * @return a new instance of ValidationError
     */
    public static ValidationError mapBindingResultToValidationError(BindingResult result) {
        // This uses a list of pairs instead of a hash map
        // because it is a neater implementation in java
        // and reading the errors array is simpler in javascript.
        var results = result
                .getAllErrors()
                .stream()
                .map(entry -> {
                    if (entry instanceof FieldError) {
                        return new Pair<>(((FieldError) entry).getField(), entry.getCode());
                    } else {
                        return new Pair<>("unknown", entry.getCode());
                    }
                })
                .collect(Collectors.toList());
        return new ValidationError(results);
    }
}
