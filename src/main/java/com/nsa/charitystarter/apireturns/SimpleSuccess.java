package com.nsa.charitystarter.apireturns;

/**
 * Used to jsonify a simple success message
 */
public class SimpleSuccess {
    /**
     * Stores the value success: true
     * Doesn't have getters/setters as it is final.
     */
    public final boolean success = true;
}
