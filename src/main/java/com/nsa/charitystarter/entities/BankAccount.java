package com.nsa.charitystarter.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.Optional;

/**
 * Represents the bank account details of a charity
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "bank_account")
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * Contains the charity the bank account is pointed to.
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "charity_id")
    private Charity charity;

    @Column(name = "sort_code")
    private int sortCode;

    @Column(name = "account_number")
    private int accountNumber;

    @Column(name = "last_valid_day")
    private Date lastValidDay;

}
