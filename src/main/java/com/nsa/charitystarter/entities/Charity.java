package com.nsa.charitystarter.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Table(name = "charity")
@Entity
public class Charity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String acronym;

    private String purpose;

    @Column(name = "logo_file_name")
    private String logoFileName;

    @Column(name = "registration_id")
    private String registrationNumber;

    @Column(name = "is_active")
    private Character isActive;
    /**
     * Contains the charity's bank account.
     */
    @OneToMany(mappedBy = "charity", targetEntity = BankAccount.class, fetch = FetchType.EAGER)
    private List<BankAccount> bankAccounts;
}
