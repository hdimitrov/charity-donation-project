package com.nsa.charitystarter.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CharityDetailsDTO {
    private String name;
    private int amountNoGiftAid;
    private int giftAidAmount;
}
