package com.nsa.charitystarter.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CharityInListDTO {
    private String name;
    private int id;
    private int totalAmount;
    private int numberOfDonations;
}
