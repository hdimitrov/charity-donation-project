<#include "base.ftl"/>
<#macro title>Add charity</#macro>
<#macro content>
<div id="add-form">
    <div class="alert alert-danger" v-if="errors.length != 0">
        Error in:
        <ul>
            <li v-for="entry in errors"> {{ entry.key }}</li>
        </ul>
    </div>
    Registration ID:
    <input type="text" class="form-control" v-model="registrationID">
    Name:
    <input type="text" class="form-control" v-model="name">
    Purpose:
    <input type="text" class="form-control" v-model="purpose">
    Sort code:
    <input type="text" class="form-control" v-model="sortCode">
    Account number:
    <input type="text" class="form-control" v-model="accountNumber">

    <button class="btn btn-primary" @click="submitForm">Add charity</button>
</div>
<script>
    let app = new Vue({
        el: '#add-form',
        data: {
            registrationID: '',
            name: '',
            purpose: '',
            sortCode: '',
            accountNumber: '',
            errors: [],
        },
        computed: {
            validData() {
                return this.registrationID && this.name && this.purpose && this.sortCode && this.accountNumber;
            },
        },
        methods: {
            submitForm() {
                if(this.validData) {
                    this.sendRequest();
                } else {
                    alert('Invalid data entered - please try again');
                }
            },
            sendRequest() {
                this.errors = [];
                let formData = new FormData();
                formData.append('registrationID', this.registrationID);
                formData.append('name', this.name);
                formData.append('purpose', this.purpose);
                formData.append('sortCode', this.sortCode);
                formData.append('accountNumber',this.accountNumber);

                window.axios.post('/api/charities', formData)
                    .then(function () {
                        alert('Successfully added charity!');
                    })
                    .catch(function( { response } ) {
                        console.log(response.data);
                        console.log(this);
                        if(response.data.errors) {
                            console.log(response.data.errors);
                            // this definition becomes window instead of the vue instance, which is why we call it directly.
                            app.errors = response.data.errors;
                        }
                    });
            }
        }
    })

</script>
</#macro>

<@page_rendered/>