<#include "base.ftl"/>
<#macro title>Add charity</#macro>
<#macro content>
<div id="view-single-charity">
    <div class="alert alert-danger" v-if="charityNotFound">
        Charity not found!
    </div>
   <table v-else class="table table-striped">
       <tr>
           <td>Charity name</td>
           <td>{{ charityName }}</td>
       </tr>
       <tr>
           <td>Donation amount with no gift aid</td>
           <td>{{ donationAmountInPounds }}</td>
       </tr>
       <tr>
           <td>Gift aid amount:</td>
           <td>{{ giftAidAmountInPounds }}</td>
       </tr>
       <tr>
           <td>Total amount</td>
           <td>{{ totalAmountInPounds }}</td>
       </tr>
   </table>
</div>
<script>
    let app = new Vue({
        el: '#view-single-charity',
        data: {
            charityName: '',
            donationAmount: 0,
            giftAidAmount: 0,
            charityNotFound: true,
        },
        computed: {
            totalAmountInPounds() {
                return "£" + ((this.donationAmount + this.giftAidAmount)/100);
            },
            donationAmountInPounds() {
                return "£" + ((this.donationAmount)/100);
            },
            giftAidAmountInPounds() {
                return "£" + ((this.giftAidAmount)/100);
            },
        },
        mounted() {
            window.axios.get('/api/charities/' + window.url(-1)).then(
                    ({ data }) => {
                        app.charityName = data.name;
                        app.donationAmount = data.amountNoGiftAid;
                        app.giftAidAmount = data.giftAidAmount;
                        app.charityNotFound = false;
            }
            ).catch( () => {
                app.charityNotFound = true;
            });
        }
    })

</script>
</#macro>

<@page_rendered/>