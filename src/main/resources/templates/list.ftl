<#include "base.ftl"/>
<#macro title>List charities</#macro>
<#macro content>
<div id="list-of-charities" class="container">
    <div class="col-md-4">
        <h2>Filters</h2>
        Search by name:
        <input type="text" v-model="nameFilter" class="form-control">
        Minimum donation amount:
        <input type="text" v-model="minDonationAmount" class="form-control">
        Minimum donation count
        <input type="text" v-model="minDonationCount" class="form-control">
        Items per page:
        <input type="text" v-model="itemsPerPage" class="form-control">
        Order By:
        <select v-model="sortBy" class="form-control">
            <option value="name">Name</option>
            <option value="total_amount">Total amount</option>
            <option value="number_of_donations">Number of donations</option>
        </select>
        Order direction:
        <select v-model="sortDirection" class="form-control">
            <option value="asc">Up</option>
            <option value="desc">Down</option>
        </select>
        <a class="btn btn-success" @click="loadData">Filter</a>




    </div>
    <div class="col-md-8">
        <div v-if="errors.length != 0" class="alert alert-danger">
            Error in field:
            <ul>
                <li v-for="error in errors"> {{error.key}} </li>
            </ul>
        </div>
        <div style="text-align:center; font-weight:bold;">Page  {{pageNumber + 1}}</div>
        <table class="table table-striped">
        <thead>
        <tr>
            <th>Charity name</th>
            <th>Total donation amount</th>
            <th>Total count</th>
        </tr>
        </thead>
        <tr v-if="itemList.length != 0" v-for=" charity in itemList" :key="charity.id">
            <td><a :href="'/charities/view/' + charity.id">{{ charity.name }}</a></td>
            <td>{{ charity.totalAmount  | inPounds}}</td>
            <td>{{ charity.numberOfDonations }}</td>
        </tr>
        <tr v-else>
            <td colspan="3">No data!</td>
        </tr>
        <tr>
        </tr>
    </table>
        <div style="text-align:center">
            <button class="btn btn-dark" @click="pageBackward"> < </button>
            <button class="btn btn-dark" @click="pageForward"> > </button>
        </div>
    </div>
</div>
<script>
    let app = new Vue({
        el: '#list-of-charities',
        data: {
            nameFilter: '',
            minDonationAmount: 0,
            minDonationCount: 0,
            itemsPerPage: 10,
            sortBy: 'total_amount',
            sortDirection: 'desc',
            pageNumber: 0,
            itemList: [],
            errors: [],
        },
        methods: {
            loadData() {
                this.errors =  [];
                window.axios.get('/api/charities',
                {
                    params: {
                        nameFilter: this.nameFilter,
                        itemsPerPage: this.itemsPerPage,
                        sortBy: this.sortBy,
                        sortDirection: this.sortDirection,
                        minimumDonationCountFilter: this.minDonationCount,
                        minimumDonationAmountFilter: this.minDonationAmount * 100 ,
                        startingPage: this.pageNumber,
                    }
                })
                .then(function(response) {
                   app.itemList = response.data;
                })
                .catch( function (error) {
                    app.errors = error.response.data.errors;
                });
            },
            pageForward() {
                this.pageNumber++;
                this.loadData();
            },
            pageBackward() {
                this.pageNumber--;
                this.loadData();
            },
        },
        mounted() {
            this.loadData();
        },
        filters: {
            inPounds(value) {
                return '£' + (value/100);
            }
        }
    })

</script>
</#macro>

<@page_rendered/>