<#macro title>
    Base
</#macro>

<#macro content>
    content lalala
</#macro>


<#macro page_rendered>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><@title/></title>

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="/js/js-url-2.5.0/url.js"></script>

</head>
<body>


<div class="container">
    <div class="header">
        <h3 class="text-muted"><@title/></h3>
    </div>


    <hr/>


    <div class="jumbotron">
        <h1>Charity Giving</h1>
        <@content/>
    </div>

</div>


</body>
</html>
</#macro>