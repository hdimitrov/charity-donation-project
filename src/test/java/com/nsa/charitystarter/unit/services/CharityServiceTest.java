package com.nsa.charitystarter.unit.services;

import com.nsa.charitystarter.dtos.CharityDetailsDTO;
import com.nsa.charitystarter.entities.BankAccount;
import com.nsa.charitystarter.entities.Charity;
import com.nsa.charitystarter.repositories.BankAccountRepository;
import com.nsa.charitystarter.repositories.CharityDetailsRepository;
import com.nsa.charitystarter.repositories.CharityRepository;
import com.nsa.charitystarter.services.CharityService;
import com.nsa.charitystarter.services.CharityServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.LinkedList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = CharityServiceImpl.class)
@DirtiesContext
public class CharityServiceTest {

    @MockBean
    private CharityRepository charityRepository;

    @MockBean
    private BankAccountRepository bankAccountRepository;
    @MockBean
    private CharityDetailsRepository charityDetailsRepository;

    @Autowired
    private CharityService charityService;

    @Test
    public void testAdd() {
        var c = new Charity();
        var b = new BankAccount();
        this.charityService.addCharity(c,b);
        verify(charityRepository).save(c);
        verify(bankAccountRepository).save(b);
    }

    @Test
    public void testRetrieve() {
        var mockReturn = Optional.of(new CharityDetailsDTO("Test",456,1));
        when(charityDetailsRepository.getCharityDTOByID(1)).thenReturn(mockReturn);
        var charityDetailsByID = this.charityService.getCharityDetailsByID(1);
        assertEquals(mockReturn, charityDetailsByID);
    }

    @Test
    public void testRetrieveNonexistent() {
        when(charityDetailsRepository.getCharityDTOByID(1)).thenReturn(Optional.empty());
        var charityDetailsByID = this.charityService.getCharityDetailsByID(1);
        assertEquals(Optional.empty(), charityDetailsByID);
    }

    @Test
    public void testMultiplicationCorrect() {
        when(charityDetailsRepository.getCharitiesInList(anyInt(), anyInt(), anyString(), anyInt(), anyInt(), anyString(), anyString()))
                .thenReturn(new LinkedList<>());
        var list = charityService.getCharitiesFiltered(0,0,"test",2,15,"name", "desc");
        assertEquals(list, new LinkedList<>());
        verify(charityDetailsRepository).getCharitiesInList(0,0,"test",30,15,"name", "desc");

    }
}
