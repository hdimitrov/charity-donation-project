package com.nsa.charitystarter.unit.repositories;

import com.nsa.charitystarter.entities.BankAccount;
import com.nsa.charitystarter.entities.Charity;
import com.nsa.charitystarter.repositories.BankAccountRepository;
import com.nsa.charitystarter.repositories.CharityRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureMockMvc
@DirtiesContext
public class BankAccountRepositoryTest {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private CharityRepository charityRepository;
    @Test
    public void testAdd() {
        var c = new Charity();
        c.setAcronym("12345");
        c.setName("123456");
        c.setRegistrationNumber("123456");
        c.setPurpose("123456");
        charityRepository.save(c);
        var b = new BankAccount();
        b.setSortCode(123456);
        b.setAccountNumber(123456);
        b.setCharity(c);
        this.bankAccountRepository.save(b);
        List<BankAccount> all = this.bankAccountRepository.findAll();
        var bankAccount = all.get(all.size() - 1);
        assertEquals(b.getSortCode(), bankAccount.getSortCode());
        assertEquals(b.getAccountNumber(), bankAccount.getAccountNumber());
        assertEquals(bankAccount.getCharity().getName(), c.getName());
    }
}
