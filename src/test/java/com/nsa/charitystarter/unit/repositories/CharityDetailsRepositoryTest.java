package com.nsa.charitystarter.unit.repositories;

import com.nsa.charitystarter.CharityApp;
import com.nsa.charitystarter.dtos.CharityDetailsDTO;
import com.nsa.charitystarter.repositories.CharityDetailsRepository;
import com.nsa.charitystarter.repositories.CharityDetailsRepositoryImpl;
import lombok.Data;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@DirtiesContext

public class CharityDetailsRepositoryTest {

    @Autowired
    private CharityDetailsRepository charityDetailsRepository;
    // Reference: http://www.javarticles.com/2016/02/spring-sql-annotation-example.html
    // Accessed on 27/10/2018
    @Test
    @Sql("/addTestCharity.sql")
    public void testRetrieveExisting() {
        var charityDTOByID = charityDetailsRepository.getCharityDTOByID(123456);
        var charityDetailsDTO = charityDTOByID.get();
        assertEquals(charityDetailsDTO.getGiftAidAmount(), 20);
        assertEquals(charityDetailsDTO.getAmountNoGiftAid(), 200);
        assertEquals(charityDetailsDTO.getName(), "Test Charity");

    }
    @Test
    public void testRetrieveNonExisting() {
        var charityDTOByID = charityDetailsRepository.getCharityDTOByID(-1);
        assertEquals(charityDTOByID, Optional.empty());
    }
    @Test
    public void testParametersPassedCorrectly() {
       var response =  charityDetailsRepository.getCharitiesInList(30,5,"cancer", 0, 3, "total_amount", "asc");
       assertTrue(response.size() <= 3);
        for(var i = 0; i<response.size(); i++) {
            if(i != 0) {
                assertTrue(response.get(i).getTotalAmount() > response.get(i-1).getTotalAmount());
            }
            response.get(i).getName().toLowerCase().contains("cancer");
            assertTrue(response.get(i).getTotalAmount() >= 30);
            assertTrue(response.get(i).getNumberOfDonations() >= 5);
        }
    }
}
