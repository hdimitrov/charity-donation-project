package com.nsa.charitystarter.unit.repositories;

import com.nsa.charitystarter.entities.Charity;
import com.nsa.charitystarter.repositories.CharityRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
//@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@DirtiesContext
public class CharityRepositoryTest {

    @Autowired
    private CharityRepository charityRepository;
    @Test
    public void testAdd() {
        var c = new Charity();
        c.setAcronym("12345");
        c.setName("123456");
        c.setRegistrationNumber("123456");
        c.setPurpose("123456");
        this.charityRepository.save(c);
        List<Charity> all = this.charityRepository.findAll();
        Charity charity = all.get(all.size() - 1);
        assertEquals(c.getAcronym(), charity.getAcronym());
        assertEquals(c.getName(), charity.getName());
        assertEquals(c.getPurpose(), charity.getPurpose());
        assertEquals(c.getRegistrationNumber(), charity.getRegistrationNumber());
    }
}
