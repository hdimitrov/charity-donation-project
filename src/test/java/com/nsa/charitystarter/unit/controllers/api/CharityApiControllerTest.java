package com.nsa.charitystarter.unit.controllers.api;

import com.nsa.charitystarter.controllers.api.CharityApiController;
import com.nsa.charitystarter.dtos.CharityDetailsDTO;
import com.nsa.charitystarter.entities.BankAccount;
import com.nsa.charitystarter.entities.Charity;
import com.nsa.charitystarter.services.CharityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.LinkedList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext
public class CharityApiControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CharityService charityService;

    @Test
    public void testAddInvalid() throws Exception {
        this
                .mvc
                .perform(
                        post("/api/charities"))
                .andDo(print())
                .andExpect(status().is(400))
                .andExpect(content().json("{\"errors\":[{\"key\":\"registrationID\",\"value\":\"NotEmpty\"},{\"key\":\"name\",\"value\":\"NotEmpty\"},{\"key\":\"purpose\",\"value\":\"NotEmpty\"}]}"));
    }

    @Test
    public void testAddValidCharity() throws Exception {
        Charity c = new Charity();
        c.setAcronym("12345");
        c.setName("123456");
        c.setRegistrationNumber("123456");
        c.setPurpose("123456");
        BankAccount b = new BankAccount();
        b.setCharity(c);
        b.setAccountNumber(123456);
        b.setSortCode(123456);
        this.mvc.perform(
                post("/api/charities")
                .param("registrationID", "123456")
                .param("name", "123456")
                .param("purpose", "123456")
                .param("sortCode", "123456")
                .param("accountNumber","123456")
        )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().json("{success:true}"));

        //Reference: https://stackoverflow.com/questions/1142837/verify-object-attribute-value-with-mockito
        // Accessed on 25/10/2018
        ArgumentCaptor<Charity> argumentCaptorCharity = ArgumentCaptor.forClass(Charity.class);
        ArgumentCaptor<BankAccount> argumentCaptorBankAccount = ArgumentCaptor.forClass(BankAccount.class);
        verify(charityService).addCharity(argumentCaptorCharity.capture(), argumentCaptorBankAccount.capture());
        BankAccount returnedBankAccount = argumentCaptorBankAccount.getValue();
        assertEquals(returnedBankAccount.getAccountNumber(),b.getAccountNumber());
        assertEquals(returnedBankAccount.getSortCode(),b.getSortCode());
        Charity returnedCharity = argumentCaptorCharity.getValue();
        assertEquals(returnedCharity.getName(), returnedCharity.getName());
        assertEquals(returnedCharity.getRegistrationNumber(), returnedCharity.getRegistrationNumber());
        assertEquals(returnedCharity.getPurpose(), returnedCharity.getPurpose());
    }

    @Test
    public void testViewValidCharity() throws Exception {
        Optional<CharityDetailsDTO> mockReturn = Optional.of(new CharityDetailsDTO("Test",456,1));
        when(charityService.getCharityDetailsByID(1)).thenReturn(mockReturn);
        this.mvc.perform(get("/api/charities/1"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().json("{\"name\":\"Test\",\"amountNoGiftAid\":456,\"giftAidAmount\":1}"));
    }

    @Test
    public void testViewInvalidCharity() throws Exception {
        Optional<CharityDetailsDTO> mockReturn = Optional.empty();
        when(charityService.getCharityDetailsByID(1)).thenReturn(mockReturn);
        this.mvc.perform(get("/api/charities/1"))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void testSearchInvalid() throws Exception {
        this
                .mvc
                .perform(
                        get("/api/charities")
                .param("minimumDonationCountFilter", "i am not a number"))
                .andDo(print())
                .andExpect(status().is(400))
                .andExpect(content().json("{\"errors\":[{\"key\":\"minimumDonationCountFilter\",\"value\":\"typeMismatch\"}]}"));
    }

    @Test
    public void testSearchValid() throws Exception {
        when(charityService.getCharitiesFiltered(anyInt(), anyInt(), anyString(), anyInt(), anyInt(), anyString(), anyString()))
                .thenReturn(new LinkedList<>());

        this
                .mvc
                .perform(
                        get("/api/charities")
                        .param("minimumDonationCountFilter", "5")
                        .param("nameFilter","Cancer")
                )
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(content().json("[]"));
        verify(charityService).getCharitiesFiltered(anyInt(), anyInt(), anyString(), anyInt(), anyInt(), anyString(), anyString());
    }


}
