package com.nsa.charitystarter.integration;

import com.nsa.charitystarter.entities.BankAccount;
import com.nsa.charitystarter.entities.Charity;
import com.nsa.charitystarter.repositories.BankAccountRepository;
import com.nsa.charitystarter.repositories.CharityRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
//Reference: https://stackoverflow.com/questions/14718088/how-do-you-reset-spring-junit-application-context-after-a-test-class-dirties-it
// Accessed on 25/10/2018
@DirtiesContext

public class AddCharityTest {


    @Autowired
    private MockMvc mvc;

    @Autowired
    private CharityRepository charityRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Test
    public void testAddCharity() throws Exception {
        Charity c = new Charity();
        c.setAcronym("12345");
        c.setName("123456");
        c.setRegistrationNumber("123456");
        c.setPurpose("123456");
        BankAccount b = new BankAccount();
//        b.setCharity(c);
        b.setAccountNumber(123456);
        b.setSortCode(123456);

        this.mvc.perform(
                post("/api/charities")
                        .param("registrationID", "123456")
                        .param("name", "123456")
                        .param("purpose", "123456")
                        .param("sortCode", "123456")
                        .param("accountNumber","123456")
        )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().json("{success:true}"));

        Boolean containsBankAccount = bankAccountRepository.findAll().stream().anyMatch(bankAccount -> {
            return bankAccount.getSortCode() == b.getSortCode() && bankAccount.getAccountNumber() == b.getAccountNumber();
        });
        assertTrue(containsBankAccount);

        Boolean containsCharity = charityRepository.findAll().stream().anyMatch(charity -> {
            return charity.getName().equals(c.getName()) && c.getAcronym().equals(charity.getAcronym()) && c.getPurpose().equals(charity.getPurpose()) && charity.getRegistrationNumber().equals(c.getRegistrationNumber());
        });
        assertTrue(containsCharity);



    }
}
