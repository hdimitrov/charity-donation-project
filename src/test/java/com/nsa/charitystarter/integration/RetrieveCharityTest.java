package com.nsa.charitystarter.integration;

import com.nsa.charitystarter.dtos.CharityDetailsDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
//Reference: https://stackoverflow.com/questions/14718088/how-do-you-reset-spring-junit-application-context-after-a-test-class-dirties-it
// Accessed on 25/10/2018
@DirtiesContext
public class RetrieveCharityTest {
    @Autowired
    private MockMvc mvc;
    @Test
    @Sql("/addTestCharity.sql")
    public void testRetrieveCharity() throws Exception {
        this.mvc.perform(get("/api/charities/123456"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().json("{\"name\":\"Test Charity\",\"amountNoGiftAid\":200,\"giftAidAmount\":20}"));
    }
}
