-- this would include more  data if this was a real database
-- such as a new donor for testing
-- but this simplifies it
INSERT INTO `charity` (`id`, `registration_id`, `name`, `purpose`, `logo_file_name`) VALUES ('123456', '123', 'Test Charity', 'Test Charity', 'test');
insert into donation (id, amount_in_pence, donation_date, is_own_money, has_no_benefit_to_donor, wishes_to_gift_aid, donor_id, sponsor_form_id, charity_id) values (NULL, 100, '2016-03-16', 1, 1, 1, 675, null, 123456);
insert into donation (id, amount_in_pence, donation_date, is_own_money, has_no_benefit_to_donor, wishes_to_gift_aid, donor_id, sponsor_form_id, charity_id) values (NULL, 100, '2016-03-16', 0, 0, 0, 675, null, 123456);

